# Market-Bot Discord Bot

[![Build Status](https://travis-ci.com/Tesshin/Market-JS.svg?token=sz9EfaY65y1dePiQtxzm&branch=master)](https://travis-ci.com/Tesshin/Market-JS)
[![discord.js 11.4.2](https://img.shields.io/badge/discord.js-11.4.2-blue.svg)](https://discord.js.org/)
[![Node.js 10.11.0](https://img.shields.io/badge/Node.js-10.11.0-green.svg)](https://nodejs.org/en/)
[![MIT License](https://img.shields.io/badge/License-MIT-blue.svg)](https://github.com/Tesshin/Market-JS/blob/master/README.md)

A Discord bot for the online virtual pet adopting website. With the bot you can view pet information and pound opening times straight in Discord without needing to access the website.

### Prerequisites

* Node 8 and above.
* A [Discord](https://discordapp.com) account.

### Installation
* Install all requirements using : `npm install`.
* Rename `settings-example.json` to `settings.json` and fill in the properties.
* [Refer to this for remaining steps](https://github.com/Tesshin/Market-Bot/wiki/Installation)

### Running

To start the bot run the following command:
```bash
npm run start
```

## Built With

* Node.js 10.11.0
* Discord.js 11.4.2
* bufferutil 3.0.5
* erlpack 0.1.0
* enmap 4.0.12
* mongodb 3.1.6

## Authors

* **Oliver Lin** - [@Tesshin](https://github.com/Tesshin)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details
